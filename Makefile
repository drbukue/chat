build:
	bundle exec rails db:migrate
	bundle install

test:
	bundle exec rspec

run:
	bundle exec rails s

alive:
	curl -H "Content-Type: application/json" "http://localhost:8081/status"
	curl -H "Content-Type: application/json" -X POST -d "{\"user\": \"batman\", \"text\":\"hello\"}" "http://localhost:8081/message"
	curl -H "Content-Type: application/json" "http://localhost:8081/messages"
	curl -H "Content-Type: application/json" "http://localhost:8081/users"

