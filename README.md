# README

This is a very simple implementation the chat API for the coding challenge for Tesla.

It was originally implemented using:

* Ruby 2.3.0
* Rails 5.0.3

There are some good guides on how to install it here:

https://gorails.com/setup/

For the sake of simplicity I decided to use sql lite for this challenge.


There is not a proper build process for this project since Ruby is interpreted, although, you can run migrations and do `bundle install` just by doing:

* `make build`

`make run` will spin off the server

`make test` will run the tests.

