* curl -H "Content-Type: application/json" http://localhost:8081/messages

```
{
    "messages": [
        {
            "timestamp": 1495401233.935255,
            "user": "batman",
            "text": "Hi!"
        },
        {
            "timestamp": 1495401915.000494,
            "user": "batman",
            "text": "\"hello!\""
        },
        {
            "timestamp": 1495401944.203273,
            "user": "batman",
            "text": "\"hello!\""
        }
    ]
}
```

* curl -X POST -H "Content-Type: application/json" --data '{"user":"superman", "text":"hello"}' http://localhost:8081/message

```
{
    "ok": true
}
```

* curl -H "Content-Type: application/json" http://localhost:8081/users

```
{
    "users": [
        "batman",
        "superman"
    ]
}
```