Rails.application.routes.draw do
  # TODO: 
  #   1.- Namespace it under /api 
  #   2.- Version it before putting it in production

  # For a REST API should be something like:
  #
  #   resources :messages, only: [:index, :create]
  #
  # example asked for POST /message instead of messages for CREATE
  # so I am providing the following implementation:
  resources :messages, only: :index
  post 'message' => 'messages#create' 

  resources :users, only: :index

  root 'application#not_found'
  match "*path", to: "application#not_found", via: :all
end
