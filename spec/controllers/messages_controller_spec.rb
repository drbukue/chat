require "rails_helper"

RSpec.describe MessagesController, :type => :controller do

  describe "#index" do

    subject{ get :index, format: :json }

    it "responds successfully with an HTTP 200 status code" do
      subject
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    describe "when users had posted messages" do
      let(:batman){ User.create username: "batman" }

      let(:sfx){ "Bam! Kapow!" }
      let(:jingle){ "Na, na, na, na, na, na, na, na, na, na, na, na, na" }

      before do
        Timecop.freeze(Date.today - 10) do
          10.times do
            Message.create(
              text: sfx,
              user: batman
            )
          end
        end

        100.times do
          Message.create(
            text: jingle,
            user: batman
          )
        end
      end

      it "only returns the latest 100" do
        subject

        messages = assigns(:messages)
        
        expect(messages.count).to eq(100)
        expect(messages.map(&:text).uniq).to match_array([jingle])
        expect(messages.map(&:text).uniq).not_to match_array([sfx])
      end
    end
    
  end

  describe "#create" do
    let(:params) do
      {
        text: "hello",
        user: username
      }
    end

    let(:username){ "batman" }

    subject{ post :create, params: params }

    describe "when the user exists" do
      let!(:user){ User.create username: username }
      
      it "returns a 200 status" do
        subject
        expect(response).to be_success
        expect(response).to have_http_status(200)
      end 
    end

    describe "when the user doesn't exists" do
      it "returns a 200 status" do
        subject
        expect(response).not_to be_success
        expect(response).to have_http_status(400)
      end 
    end
  end
end