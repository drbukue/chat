require "rails_helper"

RSpec.describe UsersController, :type => :controller do

  describe "#index" do

    subject{ get :index, format: :json }

    it "responds successfully with an HTTP 200 status code" do
      subject
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    describe "when users had posted messages" do
      let!(:batman){ User.create username: "batman" }
      let!(:superman){ User.create username: "superman" }
      let!(:aquaman){ User.create username: "aquaman" }

      before do
        Message.create(
          text: "Na, na, na, na, na, na, na, na, na, na, na, na, na",
          user: batman
        )

        Message.create(
          text: "bubbles, bubbles, bubbles, bubbles",
          user: aquaman
        )
      end

      it "only returns those whom have posted a message at least once" do
        subject
        expect(assigns(:users)).to match_array([batman, aquaman])
      end
    end

  end
end