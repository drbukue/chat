class MessagesController < ApplicationController

	def index
		count = params[:count] || 100
		@messages = Message.preload(:user).order("created_at ASC").last(count)
	end

	def create
		user = User.find_by(username: params[:user])

		@message = Message.new(
			text: params[:text],
			user: user
		)

		if @message.save
			success
		else
			invalid_record
		end
	rescue
		error
	end

	def invalid_record
	  	render status: :bad_request, json: {
	  		"error": @message.errors
	  	}
	end
	
end
