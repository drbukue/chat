class ApplicationController < ActionController::Base
  # TODO: Protect from forgery one we have an authentication mechanism
  # protect_from_forgery with: :exception
  protect_from_forgery with: :null_session

  def not_found
  	render status: :not_found, json: {
      "error": true
    }
  end

  def success
  	render status: :ok, json: {
  		"ok": true
  	}
  end

  def error
  	render status: :internal_server_error, json: {
  		"error": true
  	}
  end
end
