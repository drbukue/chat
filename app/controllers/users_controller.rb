class UsersController < ApplicationController

	def index
		@users = User.with_at_least_one_comment
	end

end
