class User < ApplicationRecord
	has_many :messages
	validates_uniqueness_of :username

	scope :with_at_least_one_comment, -> { joins(:messages).group('users.id') }
end
